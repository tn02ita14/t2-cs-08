#include  <stdio.h>

int main(void)
{
	int ch;
	double res,x;
	int res1;
	float res3;
	int n,r;
	int a,b;
	double fv,rate,pv;
	int nperiods ;
	int num;

	printf("-----------choose option----------");
	printf("1.power function");
	printf("2.gcd");
	printf("3.check prime");
	printf("4.future value");
	printf("5.present value");
	printf("6.factors ");
	printf("7.prime factors");
	printf("8.check even/odd");
	printf("9.lcm of 3 numbers");
	printf("10.factorial");
	printf("11.combination");
	printf("12.permutation");
	printf("13.fibonacci");
	scanf("%d",&ch);
	switch(ch)
	{
		case 1:
			printf("Enter numbers and its power :");
			scanf("%f %f",&x,&n);
			res = pow(x,n);
			printf("%f",res);
			break;

		case 2:
			printf("Enter two nos :");
			scanf("%d %d",&a,&b);
			res =gcd(a,b);
			printf("%d",res);
			break;

		case 3:
			printf("Enter a number to check prime :");
			scanf("%d",&n);
			res = is_prime(n);
			printf("%d",res);
			break;

		case 4:
			
			printf("Enter rate ,nperiods ,pv:");
			scanf("%f %d %f",&rate,&nperiods,&pv);
			fv = FV(rate,nperiods,pv);
			printf("%f",fv);
			break;

		case 5:
			printf("Enter rate ,nperiods ,fv:");
			scanf("%f %d %f",&rate,&nperiods,&fv);
			pv = PV(rate,nperiods,fv);
			printf("%f",pv);
			break;

		case 6:factors();
			break;

		case 7:prime_factors();
			break;

		case 8:
			printf("Enter number to check even/odd:");
			scanf("%d",&num);
			evenOdd(num);
			break;

		case 9:lcm3();
			break;

		case 10:
			printf("Enter number to find factorial:");
			scanf("%d",&num);
			res = fact(num);
			print("%d",res);
			break;
			
		case 11:
			printf("Enter nCr n and r:");
			scanf("%d %d",&n,&r);
			res = ncr(n,r);
			print("%f",res);
			break;

		case 12:
			printf("Enter nPr n and r:");
			scanf("%d %d",&n,&r);
			res = npr(n,r);
			print("%f",res);
			break;

		case 13:printf("Enter a limit:");
			fibonacci(n);
			break;
		default:
			break;
	}
    //power fucntion
    double pow(double x, int n){
        double res=1;
        for(int i=0;i<n;i++){
            res = res * x;
                
        }
        return res;
    }
    
    //gcd function
    int gcd(int a,int b){
        while (a!=b){
            if(a > b){
                a -= b;
                
            }
            else{
                b -= a;
            }
        }
        
        return a;
    }
    
    //checks for prime 
    int is_prime(unsigned int x){
        
         if (x <= 1) return 0;
         if (x % 2 == 0 && x > 2) return 0;
         for(int i = 3; i < x / 2; i+= 2)
             {
                 if (x % i == 0) return 0;
             }
         return 1;
    }

    
	//FV fucntion
    double FV(double rate, unsigned int nperiods, double PV){
        double FV = PV * pow((1+rate),nperiods);
        return FV;
        
    }
    
    // PV function
    double PV(double rate, unsigned int nperiods, double FV){
        double PV =  pow((1+rate),nperiods)/FV;
        return PV;
        
    }
    
    
    //finding and printing all factors of a numbers
    void factors(){

    	printf("Enter number: ");
    	scanf("%d",&num);
    	printf("Factors of %d are: ", num);
    	for(int i=1;i <= num;i++)
    	{
        	if (num%i == 0)
        	{
            		printf("%d ",i);
        	}
    	}
    }

    //finding and printing all prime factors of a number
    void prime_factors(){
	int num;
    	printf("Enter a number to find its prime factors:");
    	scanf("%d",&num);
    	while (num%2 == 0)
    	{
    	    printf("%d ", 2);
    	    num = num/2;
    	}
    	for (int i = 3; i <= sqrt(num); i = i+2)
    	{
        	while (num%i == 0)
        	{
            		printf("%d ", i);
            		num = num/i;
        	}
    	}
    	if (num > 2){
        	printf ("%d ", num);
	}
    }

   //checking for even/odd
   void evenOdd(int num){
	
    	printf("Enter a number:");
    	scanf("%d",&num);
    	if(num%2==0){
        	printf("Its a even number");
	}
    	else{
        	printf("Its a odd number");
	}
    }

    //lcm of 3 numbers
    void lcm3(void){
    	int a,b,c,t,k,l;
    	printf("Enter 3numbers to find lcm:");
    	scanf("%d %d %d",&a,&b,&c);
    	if(a>b)
        	l = lcm(a,b);
    	else
        	l = lcm(b,a);
    	if(l>c)
        	k= lcm(l,c);
    	else
        	k= lcm(c,l);
    	printf("LCM of three integers is %d",k);
    	

    }
	int lcm(int a,int b){
		int t = a;
		while(1){
        		if(t % b == 0 && t % a == 0)
        		break;
        		t++;
		}
	return t;
	}

    // factorial 
	int fact(int n){
		
		int i;
    		int fact = 1;
    		if (n < 0)
        		printf("negative number");
    		else
    		{
        		for(i=1; i<=n; ++i)
        		{
            			fact = fact * i;
        		}
        		
    		}	
		return fact;		
	}
	
     //combination
	float ncr(int n,int r){
		int ncr;		
		ncr=fact(n)/(fact(r)*fact(n-r));
		return ncr;
	}

     //permutation
	float npr(int n,int r){
		int npr;		
		npr=fact(n)/fact(n-r);
		return npr;
	}

     //fibonacci
	void fibonacci(int n){
		int i, num1 = 0, num2 = 1, next;
    		printf("Fibonacci Series: ");
    		for (i = 1; i <= n; ++i)
    		{
        		printf("%d, ", num1);
        		next=  num1 + num2;
        		num1 = num2;
        		num2 = next;
    		}		
	}

}

